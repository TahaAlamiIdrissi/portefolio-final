import React, { Component } from "react";
import { Grid, Cell } from "react-mdl";

class LandingPage extends Component {
  render() {
    return (
      <div style={{ width: "100%", margin: "auto" }}>
        <Grid className="landing-grid">
          <Cell col={12}>
            <img
              src="https://icon-library.net/images/hipster-icon/hipster-icon-8.jpg"
              alt="my-avatar"
              className="avatar-img"
            />
            <div className="banner-text">
              <h1>Full Stack Web Developer</h1>
              <hr />
              <p> HTML5/CSS3 | Javascript | C/C++ | Python | Java </p>
              <div className="social-links">
                {/* Linkedin */}
                <a href="#" rel="noopener noreferrer" target="_blank">
                  <i className="fa fa-linkedin-square" aria-hidden="true" />
                </a>
                {/* Github */}
                <a href="#" rel="noopener noreferrer" target="_blank">
                  <i className="fa fa-github-square" aria-hidden="true" />
                </a>
                {/* FreeCodeCamp */}
                <a href="#" rel="noopener noreferrer" target="_blank">
                  <i
                    className="fa fa-free-code-camp"
                    aria-hidden="true"
                  />
                </a>
                {/* Youtube */}
                <a href="#" rel="noopener noreferrer" target="_blank">
                  <i className="fa fa-youtube-square" aria-hidden="true" />
                </a>
              </div>
            </div>
          </Cell>
        </Grid>
      </div>
    );
  }
}

export default LandingPage;
