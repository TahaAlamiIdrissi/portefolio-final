import React from "react";
import LandingPage from "./ladingpage";
import { Switch, Route } from "react-router-dom";
import Contact from "./contact";
import Project from "./projects";
import Resume from "./resume";
import About from "./about";

const Main = () => (
  <Switch>
    <Route exact path="/" component={LandingPage} />
    <Route  path="/contact" component={Contact} />
    <Route  path="/about" component={About} />
    <Route  path="/projects" component={Project} />
    <Route  path="/resume" component={Resume} />

  </Switch>
);
export default Main;